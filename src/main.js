import "./CSS/style.css";
import "./CSS/chart.css";

import { createApp, ref } from "vue";
import App from "./App.vue";
import { createRouter, createWebHistory } from "vue-router";

import Overview from "@/Pages/Overview.vue";
import Settings from "@/Pages/Settings.vue";
import ThemeManager from "@/themeManager";
import Database from "@/database";
import Productivity from "@/Pages/Productivity/Productivity.vue";
import ProductivityOverview from "@/Pages/Productivity/ProductivityOverview.vue";
import { createPinia } from "pinia";
import { useGoalStore } from "@/Stores/goalStore";
import { useSubGoalStore } from "@/Stores/subGoalStore";
import { useTaskStore } from "@/Stores/taskStore";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/', redirect: '/productivity'},
        {path: '/overview', component: Overview},
        {path: '/settings', component: Settings},
        {path: '/productivity', component: Productivity},
        {path: '/productivity/overview/:id', component: ProductivityOverview}
    ]
});

const pinia = createPinia();
const app = createApp(App);
export const init = ref(false);

Database.init();
ThemeManager.init(app);
app.use(router);
app.use(pinia);

new Promise(async (resolve, reject) =>  {
    const goalStore = useGoalStore(),
      subGoalStore = useSubGoalStore(),
      taskStore = useTaskStore();

    goalStore.set(await Database.retrieveObjects("goals"));
    subGoalStore.set(await Database.retrieveObjects("subgoals"));
    taskStore.set(await Database.retrieveObjects("tasks"))
    taskStore.init();

    resolve(true);
}).then(() => {
    init.value = true;
})


app.mount('#app');