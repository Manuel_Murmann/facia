import { openDB } from "idb";

class Database {
    static db;
    static init() {
        this.db = openDB("PrimaryDB", 5, {
            upgrade(database, oldVersion, newVersion, transaction, event) {
                switch (oldVersion) {
                    case 0:
                        database.createObjectStore("goals", {
                            keyPath: "id"
                        });

                        const subgoalStore = database.createObjectStore("subgoals", {
                            keyPath: "id"
                        });
                        subgoalStore.createIndex("parentId", "parentId", {unique: false});

                        const taskStore = database.createObjectStore("tasks", {
                            keyPath: "id"
                        });
                        taskStore.createIndex("parentId", "parentId", {unique: false});
                }
            }
        });
    }

    static addObjectEntry(key, value) {
        this.db.then(db => {
            const objectStore = db.transaction(key, "readwrite").objectStore(key);
            // TODO: Event handling
            const request = objectStore.add(value);
        });
    }

    static updateObjectEntry(key, value) {
        this.db.then(db => {
            const objectStore = db.transaction(key, "readwrite").objectStore(key);
            // TODO: Event handling
            const request = objectStore.put(value);
        });
    }

    static removeObjectEntry(key, value) {
        this.db.then(db => {
            const objectStore = db.transaction(key, "readwrite").objectStore(key);
            // TODO: Event handling
            const request = objectStore.delete(value.id);
        })
    }

    static async retrieveObjects(key) {
        return this.db.then(db => {
            const objectStore = db.transaction(key).objectStore(key);
            return objectStore.getAll();
        });
    }

    static async retrieveIndex(store, index, key) {
        return this.db.then(db => {
            const objectStore = db.transaction(store).objectStore(store);
            const _index = objectStore.index(index);
            return _index.getAll(key);
        });
    }

    static async retrieveLimitedIndex(store, index, check) {
        let data = [];
        return this.db.then(db => {
            const objectStore = db.transaction(store).objectStore(store);
            const _index = objectStore.index(index);
            return _index.openCursor().then(function checkObject(cursor) {
                if (!cursor) {
                    return;
                }

                if (check(cursor.value)) {
                    data.push(cursor.value);
                }

                return cursor.continue().then(checkObject);
            })
        }).then(() => {
            return data;
        });
    }
}

export default Database