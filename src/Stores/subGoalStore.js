import Database from "@/database";
import { defineStore } from "pinia";
import { computed, ref, toRaw } from "vue";
import { useTaskStore } from "@/Stores/taskStore";

export const useSubGoalStore = defineStore("subGoals", () => {
  const internalSubGoals = ref([]),
        subGoals = computed(() => internalSubGoals.value);

  const tasks = useTaskStore();

  //---------------------------Setter Functions-------------------------------
  function set(values) {
    internalSubGoals.value = values;
  }

  function addSubGoal(subGoal) {
    internalSubGoals.value.push(subGoal);
    Database.addObjectEntry("subgoals", subGoal);
  }
  function removeSubGoal(subGoal) {
    const index = internalSubGoals.value.indexOf(subGoal);
    internalSubGoals.value.splice(index, 1);
    Database.removeObjectEntry("subgoals", subGoal);

    tasks.tasks.forEach(task => {
      if (task.parentId === subGoal.id) {
        tasks.removeTask(task);
      }
    });
  }

  function updateSubGoal(subGoal) {
    let index = internalSubGoals.value.findIndex(elem => elem.id === subGoal.id);

    internalSubGoals.value[index] = subGoal;
    Database.updateObjectEntry("subgoals", toRaw(subGoal));
  }

  //---------------------------Getter Functions-------------------------------

  function getById(id) {
    const value = internalSubGoals.value.find(elem => elem.id === id);
    return value === undefined ? {} : value;
  }

  function getTasks(id) {
    return tasks.tasks.filter(elem =>
      elem.parentId === id
    );
  }

  function getProgress(id) {
    return computed(() => {
      let tasks = getTasks(id);
      let length = Math.max(tasks.length, 1);
      return Math.round(tasks.filter(task => {
        return task.checked;
      }).length / length * 100);
    });
  }

  return { set, subGoals, addSubGoal, removeSubGoal, updateSubGoal, getById, getTasks, getProgress }
})