import Database from "@/database";
import { defineStore } from "pinia";
import { computed, ref, toRaw } from "vue";
import { useSubGoalStore } from "@/Stores/subGoalStore";
import { useTaskStore } from "@/Stores/taskStore";

export const useGoalStore = defineStore("goals", () => {
  const internalGoals = ref([]),
        goals = computed(() => internalGoals.value);

  const subGoals = useSubGoalStore();
  const tasks = useTaskStore();

  //--------------------------Internal Functions--------------------------------
  function cleanseData(goal) {
    goal.title = goal.title.trim();
    goal.description = goal.description.trim();

    return goal;
  }

  //---------------------------Setter Functions---------------------------------
  function set(values) {
    internalGoals.value = values;
  }

  function addGoal(goal) {
    goal = cleanseData(goal);
    internalGoals.value.push(goal);
    Database.addObjectEntry("goals", goal);
  }
  function removeGoal(goal) {
    const index = internalGoals.value.indexOf(goal);
    internalGoals.value.splice(index, 1);
    Database.removeObjectEntry("goals", goal);

    // All tasks belonging to this goal will be removed in removeSubGoal
    subGoals.subGoals.forEach(subGoal => {
      if (subGoal.parentId === goal.id) {
        subGoals.removeSubGoal(subGoal);
      }
    });
  }

  function updateGoal(goal) {
    goal = cleanseData(goal);
    let index = internalGoals.value.findIndex(elem => elem.id === goal.id);
    internalGoals.value[index] = goal;
    Database.updateObjectEntry("goals", toRaw(goal));
  }

  //---------------------------Getter Functions---------------------------------
  function getById(id) {
    return internalGoals.value.find(elem => elem.id === id);
  }

  function getSubGoals(id) {
    return computed(() => subGoals.subGoals.filter(elem =>
      elem.parentId === id
    ));
  }

  function getTasks(id, {allTime = false}={}) {
    const taskArray = allTime ? tasks.tasks : tasks.today;
    return taskArray.filter(elem => {
        const goalId = subGoals.subGoals.find(subGoal =>
          subGoal.id === elem.parentId
        ).parentId;
        return goalId === id;
      }
    );
  }

  return { set, goals, addGoal, removeGoal, updateGoal, getById, getSubGoals, getTasks }
})