import { defineStore } from "pinia";
import { ref, watchEffect } from "vue";
import { useDialogStates } from "@/Stores/dialogStates";
import { useSubGoalStore } from "@/Stores/subGoalStore";

export class GlobalMenuOption {
  title;
  callback;
  hoverOptions;
  constructor({title, callback, hoverOptions = []}) {
    this.title = title;
    this.callback = callback;
    this.hoverOptions = hoverOptions;
  }
}

export const useGlobalMenuStore = defineStore("globalMenu", () => {
  const states = useDialogStates();
  const subGoalStore = useSubGoalStore();

  let defaultOptions = [];
  const target = ref(null);
  const options = ref([]);

  watchEffect(() => {
    defaultOptions = [
      new GlobalMenuOption({title: "New Goal", callback: () => {
          states.$reset();
          states.push("createGoal");
        }}),
      new GlobalMenuOption({
        title: "New Task",
        hoverOptions: Array.from(subGoalStore.subGoals, (val) => {
          return new GlobalMenuOption({
            title: val.title,
            callback: () => {
              states.$reset();
              states.push("createTask", {
                subGoalId: val.id,
                goalId: val.parentId
              });
            }
          })
        })
      })
    ]

    options.value = defaultOptions;
  })

  function $reset() {
    target.value = null;
    options.value = defaultOptions;
  }

  return {
    target, options, $reset
  };
});