import Database from "@/database";
import { defineStore } from "pinia";
import { computed, ref, toRaw } from "vue";
import { useSubGoalStore } from "@/Stores/subGoalStore";
import { useGoalStore } from "@/Stores/goalStore";
import { dayComparator } from "@/utils";

export const useTaskStore = defineStore("tasks", () => {
  const internalTasks = ref([]),
        tasks = computed(() => internalTasks.value);

  const subGoals = useSubGoalStore();
  const goals = useGoalStore();

  function init() {
    internalTasks.value.forEach(value => {
      let taskDate = value.dueDate.toISOString().split("T")[0];
      let todayDate = new Date().toISOString().split("T")[0];

      if (taskDate < todayDate && !value.checked) {
        value.dueDate = new Date();
        Database.updateObjectEntry("tasks", toRaw(value));
        console.log("A tasks' due date was in the past, therefore it has now been set to today")
      }
    });
  }

  //---------------------------Setter Functions-------------------------------
  function set(values) {
    internalTasks.value = values;
  }
  function addTask(task) {
    internalTasks.value.push(task);
    Database.addObjectEntry("tasks", task);
  }
  function updateTask(task) {
    let index = internalTasks.value.findIndex(elem => elem.id === task.id);
    internalTasks.value[index] = task;
    Database.updateObjectEntry("tasks", toRaw(task));
  }
  function removeTask(task) {
    const index = internalTasks.value.indexOf(task);
    internalTasks.value.splice(index, 1);
    Database.removeObjectEntry("tasks", task);
  }

  function moveElement(oldIndex, newIndex) {
    const element = internalTasks.value[oldIndex];

    internalTasks.value.splice(oldIndex, 1);
    internalTasks.value.splice(newIndex, 0, element);
  }

  //---------------------------Getter Functions-------------------------------

  const today = computed(
    () => internalTasks.value.filter(
      task => {
        return dayComparator(task.dueDate, new Date()) === 0 && !task.scrapped
      }
  ));

  function getById(id) {
    return internalTasks.value.find(elem => elem.id === id);
  }

  function getGoal(task) {
    try {
      const goalId = subGoals.subGoals.find(subGoal =>
        task.parentId === subGoal.id
      ).parentId;

      return goals.getById(goalId);
    } catch (e) {
      console.log("No goal found in getGoal()");
      return undefined;
    }
  }

  return { set, tasks, init, addTask, removeTask, updateTask, moveElement, getById, getGoal, today }
})