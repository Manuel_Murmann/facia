import { ref } from "vue";
import { defineStore } from "pinia";

export const useDialogStates = defineStore("dialogStates", () => {
  // Primary dialog
  const createGoal = ref({
    visible: false,
    title: "",
    description: "",
    onCreate: (goal) => {},

    toggle({
      title = "",
      description = "",
      onCreate = (goal) => {}
    } = {}) {
      this.visible = !this.visible;
      this.title = title;
      this.description = description;
      this.onCreate = onCreate;
    }
  })

  //----------------------------------------------------------------------------
  // Primary dialog
  // => colorPicker
  const createSubGoal = ref({
    visible: false,
    goalId: "",
    title: "",
    color: "",
    currentId: "",

    toggle({
      goalId = "",
      title = "",
      color = "color-mix(in srgb, var(--primary) 5%, transparent)",
      currentId = "",
      update = true
    } = {}) {
      this.visible = !this.visible;
      if (update) {
        this.goalId = goalId;
        this.title = title;
        this.color = color;
        this.currentId = currentId;
      }
    }
  });

  //--------------------------------------------------
  // Primary dialog
  const confirmation = ref({
    visible: false,
    onConfirm: () => {},
    onCancel: () => {},

    toggle({
      onConfirm = () => {},
      onCancel = () => {}
    } = {}) {
      this.visible = !this.visible;
      this.onConfirm = onConfirm;
      this.onCancel = onCancel;
    }
  });

  //--------------------------------------------------
  // Primary dialog
  // => createTask
  const subGoalOverview = ref({
    visible: false,
    subGoalId: "",

    toggle({
      subGoalId = "",
      update = true
    } = {}) {
      this.visible = !this.visible;
      if (update) this.subGoalId = subGoalId;
    }
  });

  //----------------------------------------------------------------------------
  // Sub dialog
  // => datePicker
  const createTask = ref({
    visible: false,
    title: "",
    currentId: "",
    subGoalId: null,
    goalId: null,
    onCreate: () => {},
    onCancel: () => {},

    toggle({
      title = "",
      currentId = "",
      subGoalId = null,
      goalId = null,
      update = true,
      onCreate = () => {},
      onCancel = () => {}
    } = {}) {
      this.visible = !this.visible;
      if (update) {
        this.title = title;
        this.currentId = currentId;
        this.subGoalId = subGoalId;
        this.goalId = goalId;
        this.onCreate = onCreate;
        this.onCancel = onCancel;
      }
    }
  })

  //--------------------------------------------------
  // Sub dialog
  const colorPicker = ref({
    visible: false,
    color: "",
    onSelected: (color) => {},
    onCancel: () => {},

    toggle({
      color = "",
      onSelected = (color) => {},
      onCancel = () => {}
    } = {}) {
      this.visible = !this.visible;
      this.color = color;
      this.onSelected = onSelected;
      this.onCancel = onCancel;
    }
  });

  //--------------------------------------------------
  // Sub dialog
  const datePicker = ref({
    visible: false,
    date: new Date(),
    onSelected: (date) => {},
    onCancel: () => {},

    toggle({
      date = new Date(),
      onSelected = (date) => {},
      onCancel = () => {}
    } = {}) {
      this.visible = !this.visible;
      this.date = date;
      this.onSelected = onSelected;
      this.onCancel = onCancel;
    }
  });

  const ghost = ref({
    visible: false,

    toggle() {
      this.visible = !this.visible;
    }
  })

  function $reset() {
    queue = [];
    for (let [title, dialog] of Object.entries(dialogs)) {
      if (dialog.value.visible) {
        dialog.value.toggle({update: true});
      }
    }
  }

  let queue = [];

  function push(name, args = {}) {
    if (queue.length > 0) {
      let lastEntry = queue[queue.length-1].name;
      dialogs[lastEntry].value.toggle({update: false});
    }
    queue.push({name, args});
    dialogs[name].value.toggle(args);
  }

  function pop(count = 1) {
    let lastEntry;
    for (let i=0; i<count; i++) {
      lastEntry = queue[queue.length-1].name;
      dialogs[lastEntry].value.toggle();
      queue.pop();

      if (queue.length > 0) {
        lastEntry = queue[queue.length-1].name;
        dialogs[lastEntry].value.toggle({update: false});
      }
    }
  }

  let dialogs = {
    createGoal, createTask, createSubGoal, confirmation, subGoalOverview, colorPicker, datePicker, ghost
  };

  return {$reset, push, pop, ...dialogs};
})