export function dayComparator(date1, date2) {
  let date1Val = date1.toISOString().split("T")[0];
  let date2Val = date2.toISOString().split("T")[0];

  if (date1Val < date2Val) {
    return 1;
  } else if (date1Val > date2Val) {
    return -1;
  } else {
    return 0;
  }
}

export function dateToString(date) {
  if (dayComparator(new Date(), date) === 0) {
    return "Today";
  }

  return date.toLocaleDateString().replaceAll("/", ".");
}