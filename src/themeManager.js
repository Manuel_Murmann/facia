const root = document.querySelector(":root");

class ThemeManager {
    static app;

    static themes = {
        dusk: {
            "primary": "#fff",
            "secondary": "rgba(255, 255, 255, 0.6)",
            "background": "#000000",
            "secondary-background": "rgba(28, 0, 15, 0.75)",
            "accent": "#FA798F",
            "negative": "#9C142C",
            "background-image": `url(/Assets/Backgrounds/dusk.jpg)`
        },
        purple: {
            "primary": "#fff",
            "secondary": "rgba(255, 255, 255, 0.6)",
            "background": "#000",
            "secondary-background": "rgba(0, 0, 0, 0.75)",
            "accent": "#865af6",
            "negative": "#9C142C",
            "background-image": `url(/Assets/Backgrounds/purple.jpg)`
        }
    };
    static init(app) {
        this.app = app;

        let theme = localStorage.getItem("theme");
        if (theme === "null") {
            theme = "dusk";
        }

        this.updateTheme(theme);
        app.config.globalProperties.$themes = this.themes;
        app.config.globalProperties.$currentTheme = this.themes[theme];
    }
    static updateTheme(name) {
        localStorage.setItem("theme", name);
        for (const object in this.themes[name]) {
            root.style.setProperty(`--${object}`, this.themes[name][object]);
        }
    }
}

export default ThemeManager;